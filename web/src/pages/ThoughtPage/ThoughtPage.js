import ThoughtsLayout from 'src/layouts/ThoughtsLayout'
import ThoughtCell from 'src/components/ThoughtCell'

const ThoughtPage = ({ id }) => {
  return (
    <ThoughtsLayout>
      <ThoughtCell id={id} />
    </ThoughtsLayout>
  )
}

export default ThoughtPage
