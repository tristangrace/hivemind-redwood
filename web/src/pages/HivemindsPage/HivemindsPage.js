import HivemindsLayout from 'src/layouts/HivemindsLayout'
import HivemindsCell from 'src/components/HivemindsCell'

const HivemindsPage = () => {
  return (
    <HivemindsLayout>
      <HivemindsCell />
    </HivemindsLayout>
  )
}

export default HivemindsPage
