import ThoughtsLayout from 'src/layouts/ThoughtsLayout'
import NewThought from 'src/components/NewThought'

const NewThoughtPage = () => {
  return (
    <ThoughtsLayout>
      <NewThought />
    </ThoughtsLayout>
  )
}

export default NewThoughtPage
