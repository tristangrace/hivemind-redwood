import ThoughtsLayout from 'src/layouts/ThoughtsLayout'
import EditThoughtCell from 'src/components/EditThoughtCell'

const EditThoughtPage = ({ id }) => {
  return (
    <ThoughtsLayout>
      <EditThoughtCell id={id} />
    </ThoughtsLayout>
  )
}

export default EditThoughtPage
