import HivemindsLayout from 'src/layouts/HivemindsLayout'
import NewHivemind from 'src/components/NewHivemind'

const NewHivemindPage = () => {
  return (
    <HivemindsLayout>
      <NewHivemind />
    </HivemindsLayout>
  )
}

export default NewHivemindPage
