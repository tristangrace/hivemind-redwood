import HivemindsLayout from 'src/layouts/HivemindsLayout'
import EditHivemindCell from 'src/components/EditHivemindCell'

const EditHivemindPage = ({ id }) => {
  return (
    <HivemindsLayout>
      <EditHivemindCell id={id} />
    </HivemindsLayout>
  )
}

export default EditHivemindPage
