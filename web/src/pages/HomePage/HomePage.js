import { Link, routes } from '@redwoodjs/router'
import HivemindsLayout from 'src/layouts/HivemindsLayout'
import HivemindsCell from 'src/components/HivemindsCell'
import NewHivemind from 'src/components/NewHivemind'
import React, { useState, createContext } from "react"; //TODO STATE

export const CounterContext = createContext();

const HomePage = () => {

  //TODO STATE
  const [hivemind, setHivemind] = React.useState({"id":1, "name": "Tris"});

  return (
    <>
       <CounterContext.Provider value={[hivemind, setHivemind]}>  {/*TODO STATE */}
        <h1 className="ml-3 text-3xl font-light leading-7 text-gray-900 sm:leading-9 sm:truncate">hivemind</h1>
        <p>Hivemind Id = {hivemind.id} + {hivemind.name}</p>
        <h2 className="ml-3 text-2xl font-bold leading-7 text-gray-900 sm:leading-9 sm:truncate">Hiveminds</h2>
        <HivemindsLayout>
          <HivemindsCell />
          <NewHivemind />
        </HivemindsLayout>
      </CounterContext.Provider>

    </>
  )
}

export default HomePage
