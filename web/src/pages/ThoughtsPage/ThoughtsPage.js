import ThoughtsLayout from 'src/layouts/ThoughtsLayout'
import ThoughtsCell from 'src/components/ThoughtsCell'

const ThoughtsPage = () => {
  return (
    <ThoughtsLayout>
      <ThoughtsCell />
    </ThoughtsLayout>
  )
}

export default ThoughtsPage
