import HivemindsLayout from 'src/layouts/HivemindsLayout'
import HivemindCell from 'src/components/HivemindCell'

const HivemindPage = ({ id }) => {
  return (
    <HivemindsLayout>
      <HivemindCell id={id} />
    </HivemindsLayout>
  )
}

export default HivemindPage
