// In this file, all Page components from 'src/pages` are auto-imported. Nested
// directories are supported, and should be uppercase. Each subdirectory will be
// prepended onto the component name.
//
// Examples:
//
// 'src/pages/HomePage/HomePage.js'         -> HomePage
// 'src/pages/Admin/BooksPage/BooksPage.js' -> AdminBooksPage

import { Router, Route } from '@redwoodjs/router'

const Routes = () => {
  return (
    <Router>
      <Route path="/thoughts/new" page={NewThoughtPage} name="newThought" />
      <Route path="/thoughts/{id:Int}/edit" page={EditThoughtPage} name="editThought" />
      <Route path="/thoughts/{id:Int}" page={ThoughtPage} name="thought" />
      <Route path="/thoughts" page={ThoughtsPage} name="thoughts" />
      <Route path="/hiveminds/new" page={NewHivemindPage} name="newHivemind" />
      <Route path="/hiveminds/{id:Int}/edit" page={EditHivemindPage} name="editHivemind" />
      <Route path="/hiveminds/{id:Int}" page={HivemindPage} name="hivemind" />
      <Route path="/hiveminds" page={HivemindsPage} name="hiveminds" />
      <Route path="/" page={HomePage} name="home" />
      <Route notfound page={NotFoundPage} />
    </Router>
  )
}

export default Routes
