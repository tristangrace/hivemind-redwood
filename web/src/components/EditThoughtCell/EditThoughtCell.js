import { useMutation, useFlash } from '@redwoodjs/web'
import { navigate, routes } from '@redwoodjs/router'
import ThoughtForm from 'src/components/ThoughtForm'

export const QUERY = gql`
  query FIND_THOUGHT_BY_ID($id: Int!) {
    thought: thought(id: $id) {
      id
      body
      hivemindId
      createdAt
    }
  }
`
const UPDATE_THOUGHT_MUTATION = gql`
  mutation UpdateThoughtMutation($id: Int!, $input: UpdateThoughtInput!) {
    updateThought(id: $id, input: $input) {
      id
      body
      hivemindId
      createdAt
    }
  }
`

export const Loading = () => <div>Loading...</div>

export const Success = ({ thought }) => {
  const { addMessage } = useFlash()
  const [updateThought, { loading, error }] = useMutation(
    UPDATE_THOUGHT_MUTATION,
    {
      onCompleted: () => {
        navigate(routes.thoughts())
        addMessage('Thought updated.', { classes: 'rw-flash-success' })
      },
    }
  )

  const onSave = (input, id) => {
    const castInput = Object.assign(input, {
      hivemindId: parseInt(input.hivemindId),
    })
    updateThought({ variables: { id, input: castInput } })
  }

  return (
    <div className="rw-segment">
      <header className="rw-segment-header">
        <h2 className="rw-heading rw-heading-secondary">
          Edit Thought {thought.id}
        </h2>
      </header>
      <div className="rw-segment-main">
        <ThoughtForm
          thought={thought}
          onSave={onSave}
          error={error}
          loading={loading}
        />
      </div>
    </div>
  )
}
