import { useMutation, useFlash } from '@redwoodjs/web'
import { Link, routes, navigate } from '@redwoodjs/router'

import { QUERY } from 'src/components/HivemindsCell'

const DELETE_HIVEMIND_MUTATION = gql`
  mutation DeleteHivemindMutation($id: Int!) {
    deleteHivemind(id: $id) {
      id
    }
  }
`

const jsonDisplay = (obj) => {
  return (
    <pre>
      <code>{JSON.stringify(obj, null, 2)}</code>
    </pre>
  )
}

const timeTag = (datetime) => {
  return (
    <time dateTime={datetime} title={datetime}>
      {new Date(datetime).toUTCString()}
    </time>
  )
}

const checkboxInputTag = (checked) => {
  return <input type="checkbox" checked={checked} disabled />
}

const Hivemind = ({ hivemind }) => {
  const { addMessage } = useFlash()
  const [deleteHivemind] = useMutation(DELETE_HIVEMIND_MUTATION, {
    onCompleted: () => {
      navigate(routes.hiveminds())
      addMessage('Hivemind deleted.', { classes: 'rw-flash-success' })
    },
  })

  const onDeleteClick = (id) => {
    if (confirm('Are you sure you want to delete hivemind ' + id + '?')) {
      deleteHivemind({ variables: { id } })
    }
  }

  return (
    <>
      <div className="rw-segment">
        <header className="rw-segment-header">
          <h2 className="rw-heading rw-heading-secondary">
            Hivemind {hivemind.id} Detail
          </h2>
        </header>
        <table className="rw-table">
          <tbody>
            <tr>
              <th>Image</th>
              <td><img className="inline-block h-14 w-14 rounded-full" src="https://images.unsplash.com/photo-1472099645785-5658abf4ff4e?ixlib=rb-1.2.1&ixqx=acMubCNzxT&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80" alt=""/></td>
            </tr>
            <tr>
              <th>Id</th>
              <td>{hivemind.id}</td>
            </tr>
            <tr>
              <th>Name</th>
              <td>{hivemind.name}</td>
            </tr>
            <tr>
              <th>Profile image</th>
              <td>{hivemind.profileImage}</td>
            </tr>
            <tr>
              <th>Created at</th>
              <td>{timeTag(hivemind.createdAt)}</td>
            </tr>
          </tbody>
        </table>
      </div>
      <nav className="rw-button-group">
        <Link
          to={routes.editHivemind({ id: hivemind.id })}
          className="rw-button rw-button-blue"
        >
          Edit
        </Link>
        <a
          href="#"
          className="rw-button rw-button-red"
          onClick={() => onDeleteClick(hivemind.id)}
        >
          Delete
        </a>
      </nav>
    </>
  )
}

export default Hivemind
