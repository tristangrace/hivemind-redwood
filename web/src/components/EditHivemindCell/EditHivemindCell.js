import { useMutation, useFlash } from '@redwoodjs/web'
import { navigate, routes } from '@redwoodjs/router'
import HivemindForm from 'src/components/HivemindForm'

export const QUERY = gql`
  query FIND_HIVEMIND_BY_ID($id: Int!) {
    hivemind: hivemind(id: $id) {
      id
      name
      profileImage
      createdAt
    }
  }
`
const UPDATE_HIVEMIND_MUTATION = gql`
  mutation UpdateHivemindMutation($id: Int!, $input: UpdateHivemindInput!) {
    updateHivemind(id: $id, input: $input) {
      id
      name
      profileImage
      createdAt
    }
  }
`

export const Loading = () => <div>Loading...</div>

export const Success = ({ hivemind }) => {
  const { addMessage } = useFlash()
  const [updateHivemind, { loading, error }] = useMutation(
    UPDATE_HIVEMIND_MUTATION,
    {
      onCompleted: () => {
        navigate(routes.hiveminds())
        addMessage('Hivemind updated.', { classes: 'rw-flash-success' })
      },
    }
  )

  const onSave = (input, id) => {
    updateHivemind({ variables: { id, input } })
  }

  return (
    <div className="rw-segment">
      <header className="rw-segment-header">
        <h2 className="rw-heading rw-heading-secondary">
          Edit Hivemind {hivemind.id}
        </h2>
      </header>
      <div className="rw-segment-main">
        <HivemindForm
          hivemind={hivemind}
          onSave={onSave}
          error={error}
          loading={loading}
        />
      </div>
    </div>
  )
}
