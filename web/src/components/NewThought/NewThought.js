import { useMutation, useFlash } from '@redwoodjs/web'
import { navigate, routes } from '@redwoodjs/router'
import ThoughtForm from 'src/components/ThoughtForm'

import { QUERY } from 'src/components/ThoughtsCell'

const CREATE_THOUGHT_MUTATION = gql`
  mutation CreateThoughtMutation($input: CreateThoughtInput!) {
    createThought(input: $input) {
      id
    }
  }
`

const NewThought = () => {
  const { addMessage } = useFlash()
  const [createThought, { loading, error }] = useMutation(
    CREATE_THOUGHT_MUTATION,
    {
      onCompleted: () => {
        navigate(routes.thoughts())
        addMessage('Thought created.', { classes: 'rw-flash-success' })
      },
    }
  )

  const onSave = (input) => {
    const castInput = Object.assign(input, {
      hivemindId: parseInt(input.hivemindId),
    })
    createThought({ variables: { input: castInput } })
  }

  return (
    <div className="rw-segment">
      <header className="rw-segment-header">
        <h2 className="rw-heading rw-heading-secondary">New Thought</h2>
      </header>
      <div className="rw-segment-main">
        <ThoughtForm onSave={onSave} loading={loading} error={error} />
      </div>
    </div>
  )
}

export default NewThought
