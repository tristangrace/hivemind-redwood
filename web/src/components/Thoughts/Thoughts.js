import { useMutation, useFlash } from '@redwoodjs/web'
import { Link, routes } from '@redwoodjs/router'

import { QUERY } from 'src/components/ThoughtsCell'

const DELETE_THOUGHT_MUTATION = gql`
  mutation DeleteThoughtMutation($id: Int!) {
    deleteThought(id: $id) {
      id
    }
  }
`

const MAX_STRING_LENGTH = 150

const truncate = (text) => {
  let output = text
  if (text && text.length > MAX_STRING_LENGTH) {
    output = output.substring(0, MAX_STRING_LENGTH) + '...'
  }
  return output
}

const jsonTruncate = (obj) => {
  return truncate(JSON.stringify(obj, null, 2))
}

const timeTag = (datetime) => {
  return (
    <time dateTime={datetime} title={datetime}>
      {new Date(datetime).toUTCString()}
    </time>
  )
}

const checkboxInputTag = (checked) => {
  return <input type="checkbox" checked={checked} disabled />
}

const ThoughtsList = ({ thoughts }) => {
  const { addMessage } = useFlash()
  const [deleteThought] = useMutation(DELETE_THOUGHT_MUTATION, {
    onCompleted: () => {
      addMessage('Thought deleted.', { classes: 'rw-flash-success' })
    },
    // This refetches the query on the list page. Read more about other ways to
    // update the cache over here:
    // https://www.apollographql.com/docs/react/data/mutations/#making-all-other-cache-updates
    refetchQueries: [{ query: QUERY }],
    awaitRefetchQueries: true,
  })

  const onDeleteClick = (id) => {
    if (confirm('Are you sure you want to delete thought ' + id + '?')) {
      deleteThought({ variables: { id } })
    }
  }

  return (
    <div className="rw-segment rw-table-wrapper-responsive">
      <table className="rw-table">
        <thead>
          <tr>
            <th>Id</th>
            <th>Body</th>
            <th>Hivemind id</th>
            <th>Created at</th>
            <th>&nbsp;</th>
          </tr>
        </thead>
        <tbody>
          {thoughts.map((thought) => (
            <tr key={thought.id}>
              <td>{truncate(thought.id)}</td>
              <td>{truncate(thought.body)}</td>
              <td>{truncate(thought.hivemindId)}</td>
              <td>{timeTag(thought.createdAt)}</td>
              <td>
                <nav className="rw-table-actions">
                  <Link
                    to={routes.thought({ id: thought.id })}
                    title={'Show thought ' + thought.id + ' detail'}
                    className="rw-button rw-button-small"
                  >
                    Show
                  </Link>
                  <Link
                    to={routes.editThought({ id: thought.id })}
                    title={'Edit thought ' + thought.id}
                    className="rw-button rw-button-small rw-button-blue"
                  >
                    Edit
                  </Link>
                  <a
                    href="#"
                    title={'Delete thought ' + thought.id}
                    className="rw-button rw-button-small rw-button-red"
                    onClick={() => onDeleteClick(thought.id)}
                  >
                    Delete
                  </a>
                </nav>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  )
}

export default ThoughtsList
