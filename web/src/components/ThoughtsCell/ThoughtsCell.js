import { Link, routes } from '@redwoodjs/router'

import Thoughts from 'src/components/Thoughts'

export const QUERY = gql`
  query THOUGHTS {
    thoughts {
      id
      body
      hivemindId
      createdAt
    }
  }
`

export const Loading = () => <div>Loading...</div>

export const Empty = () => {
  return (
    <div className="rw-text-center">
      {'No thoughts yet. '}
      <Link to={routes.newThought()} className="rw-link">
        {'Create one?'}
      </Link>
    </div>
  )
}

export const Success = ({ thoughts }) => {
  return <Thoughts thoughts={thoughts} />
}
