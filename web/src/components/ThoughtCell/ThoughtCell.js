import Thought from 'src/components/Thought'

export const QUERY = gql`
  query FIND_THOUGHT_BY_ID($id: Int!) {
    thought: thought(id: $id) {
      id
      body
      hivemindId
      createdAt
    }
  }
`

export const Loading = () => <div>Loading...</div>

export const Empty = () => <div>Thought not found</div>

export const Success = ({ thought }) => {
  return <Thought thought={thought} />
}
