import { useMutation, useFlash } from '@redwoodjs/web'
import { navigate, routes } from '@redwoodjs/router'
import HivemindForm from 'src/components/HivemindForm'
import { useForm } from 'react-hook-form'


import { QUERY } from 'src/components/HivemindsCell'

const CREATE_HIVEMIND_MUTATION = gql`
  mutation CreateHivemindMutation($input: CreateHivemindInput!) {
    createHivemind(input: $input) {
      id
    }
  }
`

const NewHivemind = () => {

  const { addMessage } = useFlash()
  const formMethods = useForm()
  const [createHivemind, { loading, error }] = useMutation(
    CREATE_HIVEMIND_MUTATION,
    {
      onCompleted: () => {
        // navigate(routes.hiveminds())
        formMethods.reset()
        addMessage('Hivemind created.', { classes: 'rw-flash-success' })
      },
    }
  )

  const onSave = (input) => {
    createHivemind({ variables: { input } })
  }

  return (
    <div className="rw-segment">
      <header className="rw-segment-header">
        <h2 className="rw-heading rw-heading-secondary">New Hivemind</h2>
      </header>
      <div className="rw-segment-main">
        <HivemindForm onSave={onSave} loading={loading} error={error} formMethods={formMethods} />
      </div>
    </div>
  )
}

export default NewHivemind
