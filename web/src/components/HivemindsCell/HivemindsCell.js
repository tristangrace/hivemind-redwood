import { Link, routes } from '@redwoodjs/router'

import Hiveminds from 'src/components/Hiveminds'

export const QUERY = gql`
  query HIVEMINDS {
    hiveminds {
      id
      name
      profileImage
      createdAt
    }
  }
`

export const Loading = () => <div>Loading...</div>

export const Empty = () => {
  return (
    <div className="rw-text-center">
      {'No hiveminds yet. '}
      <Link to={routes.newHivemind()} className="rw-link">
        {'Create one?'}
      </Link>
    </div>
  )
}

export const Success = ({ hiveminds }) => {
  return <Hiveminds hiveminds={hiveminds} />
}
