import { useMutation, useFlash } from '@redwoodjs/web'
import { Link, routes } from '@redwoodjs/router'

import { CounterContext } from "src/pages/HomePage"; // TODO STATE

import { QUERY } from 'src/components/HivemindsCell'

const DELETE_HIVEMIND_MUTATION = gql`
  mutation DeleteHivemindMutation($id: Int!) {
    deleteHivemind(id: $id) {
      id
    }
  }
`

const MAX_STRING_LENGTH = 150

const truncate = (text) => {
  let output = text
  if (text && text.length > MAX_STRING_LENGTH) {
    output = output.substring(0, MAX_STRING_LENGTH) + '...'
  }
  return output
}

const jsonTruncate = (obj) => {
  return truncate(JSON.stringify(obj, null, 2))
}

const timeTag = (datetime) => {
  return (
    <time dateTime={datetime} title={datetime}>
      {new Date(datetime).toUTCString()}
    </time>
  )
}

const checkboxInputTag = (checked) => {
  return <input type="checkbox" checked={checked} disabled />
}

const HivemindsList = ({ hiveminds }) => {
  const { addMessage } = useFlash()
  const [deleteHivemind] = useMutation(DELETE_HIVEMIND_MUTATION, {
    onCompleted: () => {
      addMessage('Hivemind deleted.', { classes: 'rw-flash-success' })
    },
    // This refetches the query on the list page. Read more about other ways to
    // update the cache over here:
    // https://www.apollographql.com/docs/react/data/mutations/#making-all-other-cache-updates
    refetchQueries: [{ query: QUERY }],
    awaitRefetchQueries: true,
  })

  const onDeleteClick = (id) => {
    if (confirm('Are you sure you want to delete hivemind ' + id + '?')) {
      deleteHivemind({ variables: { id } })
    }
  }

  const [hivemind, setHivemind] = React.useContext(CounterContext); // TODO STATE
  const changeHivemind = (data) => {
    setHivemind(data);
  };

  return (
    <div className="rw-segment rw-table-wrapper-responsive">
      <table className="rw-table">
        <thead>
          <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Profile image</th>
            <th>Created at</th>
            <th>&nbsp;</th>
          </tr>
        </thead>
        <tbody>
          {hiveminds.map((hivemind) => (
            <tr key={hivemind.id} onClick={() => changeHivemind({"id":hivemind.id, "name":hivemind.name})}>
              <td><img className="inline-block h-14 w-14 rounded-full" src="https://images.unsplash.com/photo-1472099645785-5658abf4ff4e?ixlib=rb-1.2.1&ixqx=acMubCNzxT&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80" alt=""/></td>
              <td>{truncate(hivemind.id)}</td>
              <td>{truncate(hivemind.name)}</td>
              <td>{truncate(hivemind.profileImage)}</td>
              <td>{timeTag(hivemind.createdAt)}</td>
              <td>
                <nav className="rw-table-actions">
                  <Link
                    to={routes.hivemind({ id: hivemind.id })}
                    title={'Show hivemind ' + hivemind.id + ' detail'}
                    className="rw-button rw-button-small"
                  >
                    Show
                  </Link>
                  <Link
                    to={routes.editHivemind({ id: hivemind.id })}
                    title={'Edit hivemind ' + hivemind.id}
                    className="rw-button rw-button-small rw-button-blue"
                  >
                    Edit
                  </Link>
                  <a
                    href="#"
                    title={'Delete hivemind ' + hivemind.id}
                    className="rw-button rw-button-small rw-button-red"
                    onClick={() => onDeleteClick(hivemind.id)}
                  >
                    Delete
                  </a>
                </nav>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  )
}

export default HivemindsList
