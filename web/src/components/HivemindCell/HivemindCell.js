import Hivemind from 'src/components/Hivemind'

export const QUERY = gql`
  query FIND_HIVEMIND_BY_ID($id: Int!) {
    hivemind: hivemind(id: $id) {
      id
      name
      profileImage
      createdAt
    }
  }
`

export const Loading = () => <div>Loading...</div>

export const Empty = () => <div>Hivemind not found</div>

export const Success = ({ hivemind }) => {
  return <Hivemind hivemind={hivemind} />
}
