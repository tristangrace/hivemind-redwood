export const schema = gql`
  type Thought {
    id: Int!
    body: String!
    hivemind: Hivemind!
    hivemindId: Int!
    createdAt: DateTime!
  }

  type Query {
    thoughts: [Thought!]!
    thought(id: Int!): Thought
  }

  input CreateThoughtInput {
    body: String!
    hivemindId: Int!
  }

  input UpdateThoughtInput {
    body: String
    hivemindId: Int
  }

  type Mutation {
    createThought(input: CreateThoughtInput!): Thought!
    updateThought(id: Int!, input: UpdateThoughtInput!): Thought!
    deleteThought(id: Int!): Thought!
  }
`
