export const schema = gql`
  type Hivemind {
    id: Int!
    name: String!
    profileImage: String!
    thoughts: [Thought]!
    createdAt: DateTime!
  }

  type Query {
    hiveminds: [Hivemind!]!
    hivemind(id: Int!): Hivemind
  }

  input CreateHivemindInput {
    name: String!
    profileImage: String!
  }

  input UpdateHivemindInput {
    name: String
    profileImage: String
  }

  type Mutation {
    createHivemind(input: CreateHivemindInput!): Hivemind!
    updateHivemind(id: Int!, input: UpdateHivemindInput!): Hivemind!
    deleteHivemind(id: Int!): Hivemind!
  }
`
