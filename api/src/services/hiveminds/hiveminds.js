import { db } from 'src/lib/db'

export const hiveminds = () => {
  return db.hivemind.findMany()
}

export const hivemind = ({ id }) => {
  return db.hivemind.findUnique({
    where: { id },
  })
}

export const createHivemind = ({ input }) => {
  return db.hivemind.create({
    data: input,
  })
}

export const updateHivemind = ({ id, input }) => {
  return db.hivemind.update({
    data: input,
    where: { id },
  })
}

export const deleteHivemind = ({ id }) => {
  return db.hivemind.delete({
    where: { id },
  })
}

export const Hivemind = {
  thoughts: (_obj, { root }) =>
    db.hivemind.findUnique({ where: { id: root.id } }).thoughts(),
}
