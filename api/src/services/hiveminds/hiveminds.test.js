import {
  hiveminds,
  hivemind,
  createHivemind,
  updateHivemind,
  deleteHivemind,
} from './hiveminds'

describe('hiveminds', () => {
  scenario('returns all hiveminds', async (scenario) => {
    const result = await hiveminds()

    expect(result.length).toEqual(Object.keys(scenario.hivemind).length)
  })

  scenario('returns a single hivemind', async (scenario) => {
    const result = await hivemind({ id: scenario.hivemind.one.id })

    expect(result).toEqual(scenario.hivemind.one)
  })

  scenario('creates a hivemind', async (scenario) => {
    const result = await createHivemind({
      input: { name: 'String', profileImage: 'String' },
    })

    expect(result.name).toEqual('String')
    expect(result.profileImage).toEqual('String')
  })

  scenario('updates a hivemind', async (scenario) => {
    const original = await hivemind({ id: scenario.hivemind.one.id })
    const result = await updateHivemind({
      id: original.id,
      input: { name: 'String2' },
    })

    expect(result.name).toEqual('String2')
  })

  scenario('deletes a hivemind', async (scenario) => {
    const original = await deleteHivemind({ id: scenario.hivemind.one.id })
    const result = await hivemind({ id: original.id })

    expect(result).toEqual(null)
  })
})
