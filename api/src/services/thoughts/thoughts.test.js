import {
  thoughts,
  thought,
  createThought,
  updateThought,
  deleteThought,
} from './thoughts'

describe('thoughts', () => {
  scenario('returns all thoughts', async (scenario) => {
    const result = await thoughts()

    expect(result.length).toEqual(Object.keys(scenario.thought).length)
  })

  scenario('returns a single thought', async (scenario) => {
    const result = await thought({ id: scenario.thought.one.id })

    expect(result).toEqual(scenario.thought.one)
  })

  scenario('creates a thought', async (scenario) => {
    const result = await createThought({
      input: { body: 'String', hivemindId: 'scenario.thought.two.hivemindId' },
    })

    expect(result.body).toEqual('String')
    expect(result.hivemindId).toEqual('scenario.thought.two.hivemindId')
  })

  scenario('updates a thought', async (scenario) => {
    const original = await thought({ id: scenario.thought.one.id })
    const result = await updateThought({
      id: original.id,
      input: { body: 'String2' },
    })

    expect(result.body).toEqual('String2')
  })

  scenario('deletes a thought', async (scenario) => {
    const original = await deleteThought({ id: scenario.thought.one.id })
    const result = await thought({ id: original.id })

    expect(result).toEqual(null)
  })
})
