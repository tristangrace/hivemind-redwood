import { db } from 'src/lib/db'

export const thoughts = () => {
  return db.thought.findMany()
}

export const thought = ({ id }) => {
  return db.thought.findUnique({
    where: { id },
  })
}

export const createThought = ({ input }) => {
  return db.thought.create({
    data: input,
  })
}

export const updateThought = ({ id, input }) => {
  return db.thought.update({
    data: input,
    where: { id },
  })
}

export const deleteThought = ({ id }) => {
  return db.thought.delete({
    where: { id },
  })
}

export const Thought = {
  hivemind: (_obj, { root }) =>
    db.thought.findUnique({ where: { id: root.id } }).hivemind(),
}
